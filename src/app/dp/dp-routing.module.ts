import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { DrawingPadComponent } from "./dp.component";

const routes: Routes = [
    { path: "", component: DrawingPadComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class DrawingPadRoutingModule { }
