import { Component, ElementRef, ViewChild } from "@angular/core";
import { ShareFile } from '@finanzritter/nativescript-share-file';
import { registerElement } from "nativescript-angular";
import { DrawingPad } from "nativescript-drawingpad";
import { ImageSource } from "tns-core-modules";
import * as fs from 'tns-core-modules/file-system';

registerElement('DrawingPad', () => DrawingPad);

/**
 * Permet de réaliser un dessin et de le partager à l'aide du terminal
 */
@Component({
    selector: "dp",
    templateUrl: "dp.component.html"
})
export class DrawingPadComponent {

    /**
     * Référence à l'espace réservé au dessin
     */
    @ViewChild('DrawingPad', {static: true}) DrawingPad: ElementRef;

    /**
     * Service de partage
     */
    shareFile = new ShareFile();

    /**
     * Récupération du dessin
     */
    getMyDrawing() {
        const pad = this.DrawingPad.nativeElement;

        // then get the drawing (Bitmap on Android) of the drawingpad
        let drawingImage;
        pad.getDrawing().then(
            data => {
                // At this point you have a native image (Bitmap on Android or UIImage on iOS)
                // so lets convert to a NS Image using the ImageSource
                const img = new ImageSource(data); // this can be set as the `src` of an `Image` inside your NS
                drawingImage = img; // to set the src of an Image if needed.
                // now you might want a base64 version of the image
                // const base64imageString = img.toBase64String('jpg'); // if you need it as base64

                const path = fs.path.join(fs.knownFolders.documents().path, 'pocT5bDrawingPad.jpg');
                img.saveToFile(path, 'jpg');

                this.shareFile.open({ 
                    path: path, 
                    intentTitle: 'Partager votre réalisation...', // optional Android
                    // rect: { // optional iPad
                    //     x: 110,
                    //     y: 110,
                    //     width: 0,
                    //     height: 0
                    // },
                    // options: true, // optional iOS
                    // animated: true // optional iOS
                });
            },
            err => {
                console.log(err);
            }
        );
    }

    /**
     * Effacement du dessin
     */
    clearMyDrawing() {
        const pad = this.DrawingPad.nativeElement;
        pad.clearDrawing();
    }
}
