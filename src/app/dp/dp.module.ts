import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { DrawingPadRoutingModule } from "./dp-routing.module";
import { DrawingPadComponent } from "./dp.component";


@NgModule({
    imports: [
        NativeScriptCommonModule,
        DrawingPadRoutingModule
    ],
    declarations: [
        DrawingPadComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class DPModule { }
