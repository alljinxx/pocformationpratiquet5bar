import { Component } from "@angular/core";

/**
 * Page d'accueil.
 */
@Component({
    selector: "home",
    templateUrl: "home.component.html",
    styleUrls: ["./home.component.css"],
})
export class HomeComponent {
    
}
