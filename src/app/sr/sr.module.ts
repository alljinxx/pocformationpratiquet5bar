import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptFormsModule } from "nativescript-angular";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { HomeRoutingModule } from "./sr-routing.module";
import { SRComponent } from "./sr.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        HomeRoutingModule,
        NativeScriptFormsModule
    ],
    declarations: [
        SRComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class SRModule { }
