import { Component, ElementRef, ViewChild } from "@angular/core";
import { SpeakOptions, TNSTextToSpeech } from '@nativescript-community/texttospeech';
import { SpeechRecognition, SpeechRecognitionTranscription } from "nativescript-speech-recognition";

/**
 * Permet d'écouter un texte et le retranscrire dans un champ, puis de le lire.
 */
@Component({
    selector: "sr",
    templateUrl: "sr.component.html",
    styleUrls: ["./sr.component.css"],
})
export class SRComponent {

    /**
     * Référence au champ texte
     */
    @ViewChild("textField", { static: true }) textField: ElementRef<any>;

    /**
     * Service de reconnaissance vocale
     */
    private readonly speechRecognition = new SpeechRecognition();

    /**
     * Service de diction
     */
    private readonly tts = new TNSTextToSpeech();

    /**
     * Options de diction
     */
    speakOptions: SpeakOptions = {
        text: undefined,
        locale: 'fr-FR'
    }

    /**
     * Permet de lire le contenu du champ
     */
    async speak() {
        this.speakOptions.text = this.textField.nativeElement.text;
        await this.tts.speak(this.speakOptions);
    }

    /**
     * Déclenche l'écoute du texte qui sera écrit dans le champ
     */
    async listen() {
        await this.speechRecognition.startListening({
            locale: "fr-FR",
            returnPartialResults: true,
            onResult: (transcription: SpeechRecognitionTranscription) => {
                this.textField.nativeElement.text = transcription.text;
            },
            onError: (error: string | number) => {
                console.log(error);
            }
        });
    }
}
