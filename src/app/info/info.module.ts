import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { DrawingPadRoutingModule } from "./info-routing.module";
import { InfoComponent } from "./info.component";


@NgModule({
    imports: [
        NativeScriptCommonModule,
        DrawingPadRoutingModule
    ],
    declarations: [
        InfoComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class InfoModule { }
