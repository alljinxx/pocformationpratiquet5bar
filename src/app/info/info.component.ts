import { AfterViewInit, Component } from "@angular/core";
import { DeviceInfo } from "nativescript-dna-deviceinfo";
import * as geolocation from "nativescript-geolocation";
import { getWeather, showWeather } from 'nativescript-weather-api';
import { Accuracy } from "tns-core-modules/ui/enums";

/**
 * Affiche diverses informations sur le matériel, localisation et la météo.
 */
@Component({
    selector: "info",
    templateUrl: "info.component.html"
})
export class InfoComponent implements AfterViewInit {

    /**
     * Informations de géolocalisation
     */
    geoloc: geolocation.Location;

    /**
     * Informations météorologiques
     */
    meteo: any;

    /**
     * Information du support matériel
     */
    deviceInfo: any;

    async ngAfterViewInit() {

        // Récupération de la géolocalisation
        Accuracy.any
        this.geoloc = await geolocation.getCurrentLocation({
            desiredAccuracy: Accuracy.any,
            maximumAge: 5000,
            timeout: 20000
        });

        // Météo
        await getWeather({
            key: "d5afd1bc4f64a91121efc938e3da3f51",
            lat: this.geoloc.latitude,
            lon: this.geoloc.longitude,
            unit: "metric"
        });
        this.meteo = new showWeather();

        // Téléphonie
        this.deviceInfo = {};
        this.deviceInfo.freeMemory = this.getSize(DeviceInfo.freeMemory());
        this.deviceInfo.totalMemory = this.getSize(DeviceInfo.totalMemory());
        this.deviceInfo.totalStorageSpace = this.getSize(DeviceInfo.totalStorageSpace());
        this.deviceInfo.freeStorageSpace = this.getSize(DeviceInfo.freeStorageSpace());
        this.deviceInfo.deviceId = DeviceInfo.deviceId();
        this.deviceInfo.deviceName = DeviceInfo.deviceName();
        this.deviceInfo.deviceLocale = DeviceInfo.deviceLocale();
        this.deviceInfo.deviceCountry = DeviceInfo.deviceCountry();
        this.deviceInfo.timezone = DeviceInfo.timezone();
        this.deviceInfo.userAgent = await DeviceInfo.userAgent().catch(error => console.log(error));
        this.deviceInfo.appName = DeviceInfo.appName();
        this.deviceInfo.appVersion = DeviceInfo.appVersion();
        this.deviceInfo.bundleId = DeviceInfo.bundleId();
        this.deviceInfo.bundleNumber = DeviceInfo.bundleNumber();
        this.deviceInfo.systemManufacturer = DeviceInfo.systemManufacturer();
        this.deviceInfo.batteryLevel = Math.round(DeviceInfo.batteryLevel());
        this.deviceInfo.externalStoragePaths = DeviceInfo.externalStoragePaths();
        this.deviceInfo.wifiSSID = DeviceInfo.wifiSSID();
        this.deviceInfo.isPortrait = DeviceInfo.isPortrait();
        this.deviceInfo.isTablet = DeviceInfo.isTablet();
        this.deviceInfo.is24Hour = DeviceInfo.is24Hour();
        this.deviceInfo.isEmulator = DeviceInfo.isEmulator();
        this.deviceInfo.isBatteryCharging = DeviceInfo.isBatteryCharging();
    }

    /**
     * Transforme une taille de fichier en version "HumanReadable"
     * @param bytes 
     * @param decimals 
     * @returns 
     */
    formatBytes(bytes, decimals) {
        if (bytes === 0) return '0 GB'
        if (isNaN(parseInt(bytes))) return bytes
        if (typeof bytes === 'string') bytes = parseInt(bytes)
        if (bytes === 0) return '0';
        const k = 1000;
        const dm = decimals + 1 || 3;
        const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
        const i = Math.floor(Math.log(bytes) / Math.log(k));
        return `${parseFloat((bytes / k ** i).toFixed(dm))} ${sizes[i]}`;
    }

    /**
     * Récupère et transforme les chaînes de taille de fichier
     * @param bytes 
     * @returns 
     */
    getSize(bytes: number) {
        return this.formatBytes(bytes, 2);
    }
}
