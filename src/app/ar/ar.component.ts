import { Component, ElementRef, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import { AR, ARCommonNode, ARLoadedEventData, ARNodeInteraction, ARPlaneTappedEventData, ARPosition } from "nativescript-ar";
import { AndroidApplication, Application, Slider } from "tns-core-modules";

/**
 * Configuration des modèles 3d
 */
interface Models3dInterface {
    name: string;
    scale: number;
    scaleModifier: number;
    selected?: boolean;
}

/**
 * Permet d'afficher une scène de réalité augmenté et d'y insérer divers éléments virtuels.
 */
@Component({
    selector: "home",
    templateUrl: "ar.component.html",
    styleUrls: ["./ar.component.css"]
})
export class HomeComponent {

    /**
     * Référence à l'interface de configuration des objets
     */
    @ViewChild("myUIView", { static: true }) myUIView: ElementRef<HTMLInputElement>;

    /**
     * Référence à la fenêtre AR
     */
    @ViewChild("ar", { static: true }) ar: ElementRef<AR>;

    /**
     * Liste des modèles 3d
     */
    readonly models3d: Models3dInterface[] = [
        { name: "huawei_diannao", scale: 0.25, scaleModifier: 0 },
        { name: "Monitor", scale: 0.25, scaleModifier: 0 },
        { name: "Gibson_Les_Paul_Wip", scale: 0.9, scaleModifier: 0 },
        { name: "Eevee", scale: 0.3, scaleModifier: 0 },
        { name: "Suzuki", scale: 0.4, scaleModifier: 0 },
        { name: "canon", scale: 0.6, scaleModifier: 0 },
    ];

    /**
     * Modèle sélectionné
     */
    selectedModel3d: Models3dInterface = this.models3d[0];

    /**
     * Panneau de paramètres des objects
     */
    settingsUI: ARCommonNode;

    /**
     * Flag indicant si le panneau des paramètres est affiché
     */
    settingsUIVisible = false;

    /**
     * Noeud sélectionné
     */
    selectedNode: ARCommonNode;

    /**
     * Flag permettant d'afficher l'indicateur d'activité
     */
    isBusy = false;

    constructor(
        private readonly router: Router
    ) {
        this.onModel3dSelected(this.models3d[0]);

        // Gestion du bug de destruction de la scène ArCore sur Android
        if (Application.android) {
            Application.android.on(AndroidApplication.activityBackPressedEvent, args => {
                (args as any).cancel = true;
                this.backHome()
            });
        }
    }

    /**
     * Méthode permettant de revenir à l'accueil après avoir convenabelement nettoyé l'AR (sinon ça explose)
     */
    async backHome() {
        this.ar.nativeElement.disposeNativeView();
        await this.router.navigate(['/']);
    }

    /**
     * Affichage de l'interface de configuration des objets
     * @param pos Position de l'interface
     */
    showSettingsUi(pos: ARPosition) {
        this.settingsUI.moveTo(pos);
        this.settingsUI.setVisible(true);
        this.settingsUIVisible = true;
    }

    /**
     * Masquage de l'interface de configuration des objets
     */
    hideSettingsUi() {
        this.settingsUI.setVisible(false);
        this.settingsUIVisible = false;
    }

    /**
     * Déclenché lorsque la scène est entièrement chargée
     * @param args 
     */
    async arLoaded(args: ARLoadedEventData): Promise<void> {
        this.settingsUI = await args.object.addUIView({
            position: undefined,
            view: this.myUIView.nativeElement as any,
            scale: 0.2,
            onTap: () => {
                this.hideSettingsUi();
            },
        });
        this.hideSettingsUi();
    }

    /**
     * Sélection d'un modèle
     * @param model3d 
     */
    onModel3dSelected(model3d: Models3dInterface) {
        this.selectedModel3d = model3d;
        this.models3d.map((obj) => {
            obj.selected = obj.name === this.selectedModel3d.name;
        });
    }

    /**
     * Plan cliqué
     * @param args 
     */
    async onPlaneTapped(args: ARPlaneTappedEventData): Promise<void> {

        // Si le panneau est déjà affiché, on le ferme
        if (this.settingsUIVisible) {
            this.hideSettingsUi();
            return;
        }

        // Si on est déjà en train d'ajouter un objet
        if (this.isBusy) {
            return;
        }

        // Ajout d'un modèle
        this.isBusy = true;
        const ar: AR = args.object;
        await ar.addModel({
            name: `${this.selectedModel3d.name}.glb`,
            position: args.position,
            scale: this.selectedModel3d.scale,
            mass: 20,
            rotatingEnabled: false,
            scalingEnabled: false,
            draggingEnabled: false,
            onLongPress: (interaction: ARNodeInteraction) => {
                // Suppression d'un modèle
                interaction.node.remove();
                this.hideSettingsUi();
            },
            onTap: async (interaction: ARNodeInteraction) => {
                if (this.selectedNode === interaction.node) {
                    // Désélection d'un modèle
                    this.selectedNode = undefined;
                    this.hideSettingsUi();
                } else {
                    // Sélection d'un modèle
                    this.selectedNode = interaction.node;
                    this.showSettingsUi({
                        x: interaction.node.position.x,
                        y: interaction.node.position.y + 0.5,
                        z: interaction.node.position.z + 0.5,
                    });
                }
            },
        });
        this.isBusy = false;
    }

    /**
     * Déclenché grace au slider
     * @param args 
     */
    onSizeChanged(args) {
        let slider = <Slider>args.object;
        this.selectedNode.scaleTo(
            this.selectedModel3d.scale + slider.value / 100
        );
    }

    /**
     * Déclenché par les boutons de rotation
     * @param gauche 
     */
    onRotateClicked(gauche: boolean) {
        this.selectedNode.rotateBy({ x: 0, y: 2 * (gauche ? -5 : 5), z: 0 });
    }
}
