import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { RouterModule } from "@angular/router";
import { registerElement } from "nativescript-angular";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { HomeRoutingModule } from "./ar-routing.module";
import { HomeComponent } from "./ar.component";

registerElement("AR", () => require("nativescript-ar").AR);

@NgModule({
    imports: [
        NativeScriptCommonModule,
        HomeRoutingModule,
        RouterModule
    ],
    declarations: [
        HomeComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class ARModule { }
