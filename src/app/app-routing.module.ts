import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

const routes: Routes = [
    { path: "", redirectTo: "/home", pathMatch: "full" },
    { path: "home", loadChildren: () => import("~/app/home/home.module").then((m) => m.HomeModule) },
    { path: "ar", loadChildren: () => import("~/app/ar/ar.module").then((m) => m.ARModule) },
    { path: "sr", loadChildren: () => import("~/app/sr/sr.module").then((m) => m.SRModule) },
    { path: "bs", loadChildren: () => import("~/app/dp/dp.module").then((m) => m.DPModule) },
    { path: "info", loadChildren: () => import("~/app/info/info.module").then((m) => m.InfoModule) }
];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})
export class AppRoutingModule { }
