# POC Formation pratique TSO5b - Alexandre Ruiz

Démonstrateur technique de la technologie NativeScript, réalisé par Alexandre Ruiz dans le cadre de la formation pratique du cours probatoire TSO5Bis par le CFD de Bourges (2022) au profit du CDAD de Toulon.

## Description

Cette preuve de concept permet d'illustrer l'utlisation de NativeScript dans le cadre du développement d'une application mobile.
Bien qu'elle soit compatible avec IOS, cette application n'a pu être testée que sur Android en raison d'un défaut de disponibilité de matériel Apple (Mac pour la compilation et IOS pour les test).

Cette application propose des fonctionnalités qui mettent en ouvre l'API du terminal hôte (Android/IOS) de façon native. Ceci afin de mettre en évidence les avantages de la technologie NativeScript par raport à une solution Web classique, qui, aussi perfectionnée soit-elle (PWA par exemple), reste limitée par les capacités des navigateurs qui l'héberge.

Ce projet utilise NativeScript en version 6 car elle offre, à ce jour, la meilleur compatibilté pour une démonstration de réalité augmentée utilisant la technologie ARCore d'Android.
Il est déployable sur mobile compatible ou émulateur.

**Veuillez bien noter que ce projet n'est qu'un démonstrateur, il n'a pas vocation à aller en production en l'état !**

## Configuration requise

Afin de compiler et déployer correctement ce projet, il est nécessaire d'installer l'environnment Android. Un tutoriel est disponible dans la [documentation officielle NativeScript](https://docs.nativescript.org/environment-setup.html). Comme décrite, l'installation d'Android Studio facilite grandement la configuration et est donc recommandée.
Dans le SDK Manager d'Android, il est préconisé d'utiliser le Android SDK build-tools 29 pour la compatibilité de l'ARCore (onglet SDK Tools). Si vous souhaitez executer le démonstrateur sur un émulateur Android, il faut sélectionner une version avec Google Play (la configuration utilisée pour le développement et la présentation est un Pixel 4 avec l'API 29 d'Android).

Vous devez aussi disposer de [Volta Packager](https://volta.sh/) ou de NodeJs 14 pour la compilation Angular.

## Démarrage de la démo

Une fois les sources téléchargées (suite à un git clone ou à la décompression du fichier .zip) voici les étapes à suivre (les commandes sont à réaliser à la racine du projet) :
- Lancer la commande "npm i" pour installer les dépendances.
- Lancer votre émulateur Android ou brancher un téléphone avec le deboguage USB activé (voir la doc de NativeScript).
- Lancer la commande "npm run start"

Si vous souhaitez générer un fichier APK pour déployer l'application sans connecter, vous pouvez utiliser la commande "npm run build". Le fichier généré se trouvera alors dans le chemin "platforms\android\app\build\outputs\apk\debug\app-debug.apk".